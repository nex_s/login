//
//  AddUserViewController.swift
//  Login
//
//  Created by nsn on 1/25/19.
//  Copyright © 2019 nex sn. All rights reserved.
//

import UIKit
import CoreData

class AddUserViewController: UIViewController, NSFetchedResultsControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!

    public var managedObjectContext: NSManagedObjectContext? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        userName.delegate = self
        password.delegate = self
    }
    
    @IBAction func goBack(_ sender: Any) {

        if(password.text?.count ?? 0 < 5){
            self.showAlert(title: "Input Error", message: "Password must be between 5 and 12 characters in length")
            return
        }

        let charactersetLater = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let charactersetNumber = CharacterSet(charactersIn: "0123456789")

        if (password.text?.rangeOfCharacter(from: charactersetLater) == nil || password.text?.rangeOfCharacter(from: charactersetNumber) == nil) {
            self.showAlert(title: "Input Error", message: "Password must consist of a mixture of letters and numerical digits only")
            return
        }
        self.insertNewObject((Any).self)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    @objc
    func insertNewObject(_ sender: Any) {
        let context = self.fetchedResultsController.managedObjectContext
        let newEvent = Event(context: context)

        newEvent.timestamp = Date()
        newEvent.userName = userName.text
        newEvent.password = password.text

        // Save the context.
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }

    // MARK: - Fetched results controller
    var fetchedResultsController: NSFetchedResultsController<Event> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        fetchRequest.fetchBatchSize = 20
        let sortDescriptor = NSSortDescriptor(key: "userName", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController

        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }

        return _fetchedResultsController!
    }

    var _fetchedResultsController: NSFetchedResultsController<Event>? = nil

    // MARK: -
    func textFieldDidBeginEditing(_ textField: UITextField){}

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == password){
            let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            if string.rangeOfCharacter(from: characterset.inverted) != nil {
                self.showAlert(title: "Input Error", message: "Password must consist of a mixture of letters and numerical digits only")
            }
            if((password.text?.count ?? 0) >= 12){
                self.showAlert(title: "Input Error", message: "Password must be between 5 and 12 characters in length")
            }
            if((password.text?.count ?? 0) >= 1 && String((password.text?.last)!) == string){
                self.showAlert(title: "Input Error", message: "Password must not contain any sequence of characters immediately followed by the same sequence")
            }
        }
    return true
    }

    func showAlert (title t:String, message m:String){

        password.shake()
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.red.cgColor
        password.layer.cornerRadius = 6

        let alertController = UIAlertController(title: t, message: m, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(actionOk)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UITextField {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

