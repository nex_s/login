//
//  DetailViewController.swift
//  Login
//
//  Created by nsn on 1/25/19.
//  Copyright © 2019 nex sn. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                //label.text = detail.timestamp!.description
                if let username = detail.userName?.description {
                    let dFormater = DateFormatter()
                    dFormater.dateFormat = "MM|dd|yyyy-HH:mm:ss"
                    let strDate = dFormater.string(from: detail.timestamp!)
                    label.text = "User Name:  " + username + "\n Created on:  " + strDate
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    var detailItem: Event? {
        didSet {
            // Update the view.
            configureView()
        }
    }
}

